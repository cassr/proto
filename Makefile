all: grpc proxy

grpc:
	-mkdir cassr
	rm -rf /tmp/grpcg
	git clone --depth=1 https://github.com/grpc-ecosystem/grpc-gateway /tmp/grpcg
	protoc -I. -I/tmp/grpcg/third_party/googleapis --go_out=plugins=grpc:cassr cassr.proto

proxy:
	-mkdir cassr
	go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go get -u github.com/golang/protobuf/protoc-gen-go
	rm -rf /tmp/grpcg
	git clone --depth=1 https://github.com/grpc-ecosystem/grpc-gateway /tmp/grpcg
	protoc -I. -I/tmp/grpcg/third_party/googleapis --grpc-gateway_out=logtostderr=true:cassr cassr.proto
